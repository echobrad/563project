#include <omp.h>
#include <cstdio>
#include <map>
#include <queue>
#include <string>
#include <time.h>
#include <fstream>
#include <algorithm>
#include <iostream>

using namespace std;

bool charRemove(int i)
{
    return !std::isalnum(i);
}

int charConvert(int val)
{
    if (val >= 'a' && val <= 'z')
        return val - 'a';
    if (val >= '0' && val <= '9')
        return val - '0' + 26;
    return 0;
}

int findReducer(int val, int threads)
{
    int num_per_core = (36 + threads - 1) / threads;
    int pid = (val + num_per_core)/ num_per_core - 1;
    return pid;
}

int main(int argc, char *argv[])
{

    /* Initial */
    int threads = 8;
    queue<string> files;
    queue<string> words;
    queue<map<string, int> > reducer_queue[threads][threads];
    map<string, int> reduce_count[threads];

    omp_set_num_threads(threads);

    bool writer = false;
#pragma omp parallel shared(files, reduce_count, writer) private(words)
    {
        int pid = omp_get_thread_num();
        if (pid == 0) {
            for (int i = 1; i< argc; i++) {
                files.push(argv[i]);
            }
        }
#pragma omp barrier
        /* reader process */
        while(!files.empty()) {
            string path = "";
#pragma omp critical
            {
                if (!files.empty()) {
                    path = files.front();
                    files.pop();
                }
            }
            if (path.compare("") != 0) {
                ifstream file;
                file.open(path.c_str());
                string word;
                int word_count = 0;
                while(file>>word) {
                    transform(word.begin(), word.end(), word.begin(), ::tolower);
                    word.erase(remove_if(word.begin(), word.end(), charRemove), word.end());
                    if (word.size() > 0) {
                        word_count++;
                        words.push(word);
                    }
                }
                file.close();
            }
        }

#pragma omp barrier
        /* map process */
        map<string, int> words_count;
        while (!words.empty()) {
            string word = "";
            if (!words.empty()) {
                word = words.front();
                words.pop();
            }

            if (word.compare("") != 0) {
                words_count[word]++;
            }
        }

        for (map<string,int>::iterator it = words_count.begin(); it != words_count.end(); ++it) {
            string key = it->first;
            int val = it->second;
            int converted = charConvert(key.at(0));
            int dest = findReducer(converted, threads);
            map<string, int> item;
            item[key] = val;
            //cout<<key<<"=>"<<val<<"=>"<<converted<<"=>"<<dest<<endl;
            reducer_queue[dest][pid].push(item);
        }
#pragma omp barrier
        /* reduce process */

        for (int i = 0; i < threads; i++) {
            while (!reducer_queue[pid][i].empty()) {
                map<string, int> item = reducer_queue[pid][i].front();
                reducer_queue[pid][i].pop();
                map<string, int>::iterator it = item.begin();
                //cout<<it->first<<"=>"<<it->second<<endl;
                reduce_count[pid][it->first] += it->second;
            }
        }

#pragma omp barrier
        /* write process */
#pragma omp critical
        {
            fstream output;
            if (!writer) {
                writer = true;
                output.open("result.out", fstream::out);
                output <<"Word count result:"<<endl;
            } else {
                output.open("result.out", fstream::out | fstream::app);
            }

            for (map<string, int>::iterator it = reduce_count[pid].begin(); it != reduce_count[pid].end(); ++it) {
                string key = it->first;
                int value = it->second;
                output<<key<<" : "<<value<<endl;
            }
            output.close();
        }
    }
    return 0;
}
